package afval

import (
	"log"
	"net/http"
)

// Afval
type Afval struct {
}

// New instantiates a configured Afval server.
func New() *Afval {
	afval := Afval{}
	http.HandleFunc("/", afval.handleHome)
	return &afval
}

// Start starts listening.
func (a *Afval) Start() error {
	log.Println("Start")
	return http.ListenAndServe("", nil)
}

func (a *Afval) handleHome(w http.ResponseWriter, r *http.Request) {
}
