package main

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	ics "github.com/arran4/golang-ical"
	"gitlab.com/bazzz/cfg"
	bazzzlog "gitlab.com/bazzz/log"
	"gitlab.com/bazzz/textparse"
)

const afvalwijzerURL = "https://www.mijnafvalwijzer.nl"

func main() {
	bazzzlog.Connect()
	log.SetOutput(bazzzlog.Default())
	config, err := cfg.New()
	if err != nil {
		log.Fatal("ERROR: could not load config:", err)
	}
	postcode := config.Get("Postcode")
	number := config.Get("Number")
	extension := config.Get("Extension")
	alarmHours := config.Get("AlarmHours")
	radicaleURL := config.Get("RadicaleURL")
	user := config.Get("User")
	password := config.Get("Password")
	name := config.Get("Name")
	color := config.Get("Color")
	hours, err := strconv.Atoi(alarmHours)
	if err != nil {
		log.Fatal("ERROR: config AlarmHours is not an integer")
	}
	body := getCalendar(postcode, number, extension)
	body = addAlarms(body, strconv.Itoa(hours))

	client := &http.Client{Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}}
	request, err := http.NewRequest(http.MethodPut, radicaleURL, strings.NewReader(body))
	if err != nil {
		fmt.Println(err)
	}
	request.SetBasicAuth(user, password)
	response, err := client.Do(request)
	if err != nil {
		log.Fatal("ERROR uploading ics to Radicale:", err)
	}
	if response.StatusCode != 201 {
		log.Fatal("ERROR uploading ics to Radicale: Status:", response.Status)
	}
	body = `<?xml version="1.0" encoding="UTF-8" ?><propertyupdate xmlns="DAV:" xmlns:C="urn:ietf:params:xml:ns:caldav" xmlns:CR="urn:ietf:params:xml:ns:carddav" xmlns:I="http://apple.com/ns/ical/" xmlns:INF="http://inf-it.com/ns/ab/"><set><prop><C:supported-calendar-component-set><C:comp name="VEVENT" /><C:comp name="VJOURNAL" /><C:comp name="VTODO" /></C:supported-calendar-component-set><displayname>` + name + `</displayname><I:calendar-color>` + color + `</I:calendar-color></prop></set><remove><prop><INF:addressbook-color /><CR:addressbook-description /><C:calendar-description /></prop></remove></propertyupdate>`
	request, err = http.NewRequest("PROPPATCH", radicaleURL, strings.NewReader(body))
	if err != nil {
		fmt.Println(err)
	}
	request.SetBasicAuth(user, password)
	response, err = client.Do(request)
	if err != nil {
		log.Fatal("ERROR sending proppatch to Radicale:", err)
	}
	if response.StatusCode != 207 {
		log.Fatal("ERROR sending proppatch to Radicale: Status:", response.Status)
	}
	log.Print("Afval calendar updated")
}

func getCalendar(postcode, number, extension string) string {
	cal := ics.NewCalendar()
	cal.SetMethod(ics.MethodRequest)
	response, err := http.Get(afvalwijzerURL + "/nl/" + postcode + "/" + number + "/" + extension)
	if err != nil {
		print(err)
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		print(err)
	}
	text := string(data)
	yearText := textparse.Take(text, `<div id="jaar-`, `"`)
	year, err := strconv.Atoi(yearText)
	if err != nil {
		fmt.Println(err)
	}
	items := textparse.Itemize(text, ` class="ophaaldagen">`, `<div id="calendarMessage`, `role='presentation'`)
	for i, item := range items {
		dateText := textparse.Take(item, `span-line-break">`, `</span`)
		if dateText == "" {
			continue
		}
		date, err := convertDate(dateText, year)
		if err != nil {
			fmt.Println(err)
		}
		description := textparse.Take(item, `<span class="afvaldescr">`, `</span>`)
		event := cal.AddEvent("id" + strconv.Itoa(i))
		event.SetCreatedTime(time.Now())
		event.SetDtStampTime(time.Now())
		event.SetModifiedAt(time.Now())
		event.SetStartAt(date)
		event.SetEndAt(date.Add(30 * time.Minute))
		event.SetSummary(description)
	}
	return cal.Serialize()
}

func addAlarms(input string, hours string) string {
	result := strings.ReplaceAll(input, "\r\n", "\n")
	return strings.ReplaceAll(result, "END:VEVENT\n", "BEGIN:VALARM\nTRIGGER:-PT"+hours+"H\nDESCRIPTION:Reminder\nACTION:DISPLAY\nEND:VALARM\nEND:VEVENT\n")
}

func convertDate(input string, year int) (time.Time, error) {
	parts := strings.Split(input, " ")
	if len(parts) != 3 {
		return time.Date(0, 0, 0, 0, 0, 0, 0, time.Local), errors.New("input should be of 3 parts seperated by spaces, not: " + input)
	}
	day, err := strconv.Atoi(parts[1])
	if err != nil {
		return time.Date(0, 0, 0, 0, 0, 0, 0, time.Local), fmt.Errorf("could not parse day: %w", err)
	}
	month := 0
	for i, m := range []string{"", "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"} {
		if parts[2] == m {
			month = i
			break
		}
	}
	return time.Date(year, time.Month(month), day, 8, 0, 0, 0, time.Local), nil
}
