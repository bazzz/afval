module gitlab.com/bazzz/afval

go 1.20

require (
	github.com/arran4/golang-ical v0.1.0
	gitlab.com/bazzz/cfg v0.0.0-20221031102215-280e27138222
	gitlab.com/bazzz/log v0.0.0-20220303073850-7bf406fe6b81
	gitlab.com/bazzz/textparse v0.0.0-20230221085845-cd4301ef17d1
)
